<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container_user">
  <br>
  <div class="panel-group" id="accordion_user">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_user" href="#adminfeatureusersearch">Search users</a>
        </h4>
      </div>
      <div id="adminfeatureusersearch" class="panel-collapse collapse ">
        <div class="panel-body"><?php include 'admin_user/adminfeatureusersearch.php';?></div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_user" href="#adminfeatureuseradd">Add users</a>
        </h4>
      </div>
      <div id="adminfeatureuseradd" class="panel-collapse collapse">
        <div class="panel-body"><?php include 'admin_user/adminfeatureuseradd.php';?></div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion_user" href="#adminfeatureuseredit">Edit and delete users</a>
        </h4>
      </div>
      <div id="adminfeatureuseredit" class="panel-collapse collapse">
        <div class="panel-body"><?php include 'admin_user/adminfeatureuseredit.php';?></div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
